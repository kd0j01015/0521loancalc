package test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import main.AnnualException;
import main.Loancalc;

import org.junit.Test;

public class TestLoancalc {

	@Test
	public void annual() {
		Loancalc lc = new Loancalc();
		lc.setAnnual(12);
		assertThat(lc.getAnnual(),is(12));
		lc.setAnnual(18);
		assertThat(lc.getAnnual(), is(18));		
	}
	
	@Test(expected = AnnualException.class)
	public void annualerror() {
		Loancalc lc = new Loancalc();
		lc.setAnnual(-1);
	}

}
