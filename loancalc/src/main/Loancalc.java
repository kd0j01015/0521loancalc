package main;

public class Loancalc {

	private Integer annual;

	public Integer getAnnual() {
		return annual;
	}

	public void setAnnual(Integer annual) {
		if(annual < 0) {
			throw new AnnualException();
		}
		this.annual = annual;
	}
}
